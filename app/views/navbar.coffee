`import Ember from 'ember'`
`import FoundationMixin from '../mixins/foundation'`

NavbarView = Ember.View.extend FoundationMixin,
  tagName: "nav"
  classNames: ["top-bar"]
  attributeBindings: ["isDataTopBar:data-topbar",  "role"]
  isDataTopBar: "data-topbar"
  role: "navigation"
  templateName: 'navbar'
  

  
  
`export default NavbarView`
