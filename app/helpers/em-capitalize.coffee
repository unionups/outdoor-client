`import Ember from 'ember'`

emCapitalize = (value) ->
  return value.toUpperCase()

EmCapitalizeHelper = Ember.Handlebars.makeBoundHelper emCapitalize

`export { emCapitalize }`

`export default EmCapitalizeHelper`
