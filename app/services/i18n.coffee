`import service from 'ember-cli-i18n/services/i18n'`

service.getLocalizedPath = (locale, path, container)->
  translation = container.lookupFactory('i18n:translation').translation;
  return  Ember.get(translation[locale], path)


`export default service`