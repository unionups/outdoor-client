`import Ember from 'ember'`
`import ApplicationRouteMixin from 'simple-auth/mixins/application-route-mixin'`
`import LoadingSliderMixin from '../mixins/loading-slider'`


ApplicationRoute = Ember.Route.extend ApplicationRouteMixin, LoadingSliderMixin, 
  beforeModel: ()->
    this._super()
    lang = this.session.get("content.lang")
    if lang?
      application = this.container.lookup('application:main')
      unless application.defaultLocale is lang
        set = Ember.set
        set application, 'defaultLocale', lang
        this.controller.set "lang", lang
        Ember.$.getJSON "/locales/"+application.defaultLocale+".json", (translation)=>
          this.container.lookup('i18n:translation').translation = translation




  actions:
    login: ()->
      unless this.session.isAuthenticated
        this.render "login",
          into: "application",
          outlet: 'modal',
          controller: "login"
        this.send "openModal"
    error: (error, transition)->
      switch error.status
        when 401
          this.send "login"
        when 403
          this.controller.set "modalContent", "You do not have sufficient permissions to access this page!" 
          this.send "openModal"
        # else 
        #   if error
        #     this.controller.set "modalContent", error
        #     this.send "openModal"
      this._super(error, transition)
    closeModal: ()->
      this.controller.set "isOpenModal", false
      this.controller.set "modalContent", ""
      this.disconnectOutlet
        outlet: 'modal',
        parentView: 'application'
    openModal: ()->
      this.controller.set "isOpenModal", true

    changeLang: (lang)->
      application = this.container.lookup('application:main')
      unless application.defaultLocale is lang
        Foundation.utils.S("li#"+application.defaultLocale).removeClass "active"
        Foundation.utils.S("li#"+lang).addClass "active"
        this.controller.set "lang", lang
        Ember.$.getJSON "/locales/"+lang+".json", (translation)=>
          this.container.lookup('i18n:translation').translation = translation
          set = Ember.set
          set(application, 'defaultLocale', lang)
          set(application, 'locale', lang)
          this.session.set "content.lang", lang
         

  # renderTemplate: ()->
  #   _this=this
  #   this._super()
  #   this.store.find('country').then (countries)->
  #     _this.render 'locations',
  #       into: "application"
  #       outlet: 'locations'
  #       controller: "locations"
  #       model: countries

    
    # this.render('routs', {
    #   into: 'application',
    #   outlet: 'routs',
    #   controller: 'routs'
    

  
`export default ApplicationRoute`