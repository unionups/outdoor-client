`import Ember from 'ember'`
`import AuthenticatedRouteMixin from 'outdoor-client/mixins/authenticated-route'`


RoutesNewRoute = Ember.Route.extend AuthenticatedRouteMixin, 
  
  # beforeModel: ()->
  #   this._super() 
  #   this.loadGoogleMap()

  ability: "user"
  
  isCountryChoosed: Ember.computed.alias "controller.isCountryChoosed"
  isRegionChoosed: Ember.computed.alias "controller.isRegionChoosed"
  isSubregionChoosed: Ember.computed.alias "controller.isSubregionChoosed"

  isCountryPropertiesChanged: Ember.computed.alias "controller.isCountryPropertiesChanged"
  isRegionPropertiesChanged: Ember.computed.alias "controller.isRegionPropertiesChanged"
  isSubregionPropertiesChanged: Ember.computed.alias "controller.isSubregionsPropertiesChanged"

  countryId: null
  regionId: null
  subregionId: null

  country: Ember.computed.alias "controller.country"
  region: Ember.computed.alias "controller.region"
  subregion: Ember.computed.alias "controller.subregion"


  current: Ember.computed.alias "controller.current"



  chooseLocation: (locationKey, isChoosed)->
    this.set "is"+locationKey.charAt(0).toUpperCase()+locationKey.substr(1)+"Choosed" , isChoosed


  clearLocation: (locationKey)->
    this.set locationKey, null
    this.chooseLocation locationKey , false


  loadLocation: (locationKey)->
    this.send "loading"
    this.store.find(locationKey, this.get(locationKey+"Id")).then (location)=>
      this.set locationKey, location
      this.chooseLocation locationKey , true
      this.send 'finished'
    , =>
      this.send 'finished' 


  countryIdObserve: (->
    if this.get("countryId") then this.loadLocation("country") else this.clearLocation("country")
    ).observes "countryId"

  regionIdObserve: (->
    if this.get("regionId") then this.loadLocation("region") else this.clearLocation("region")
    ).observes "regionId"

  subregionIdObserve: (->
    if this.get("subregionId") then this.loadLocation("subregion") else this.clearLocation("subregion")
    ).observes "subregionId"



  afterModel: ()->
    this.transitionTo "routes.new.country"
 

  actions:
    locationChoosed: (locationName, locationId)->
      this.set(locationName+"Id", locationId) 
      switch locationName
        when "country"
          this.set "regionId", null
          this.set "subregionId", null
          this.set "current", "region"
          this.transitionTo "routes.new.region"
        when "region"
          this.set "subregionId", null
          this.set "current", "subregion"
          this.transitionTo "routes.new.subregion"
        when "subregion"
          this.set "current", ""
          if this.get("isCountryChoosed") and this.get("isRegionChoosed") and this.get("isSubregionChoosed")
            alert "OK!"


`export default RoutesNewRoute`