`import Ember from 'ember'`
`import ChooseLocationRouteMixin from "outdoor-client/mixins/choose-location-route"`
`import ManageTypedInsertionsRouteMixin from 'outdoor-client/mixins/manage-typed-insertions-route'`

RoutesNewSubregionRoute = Ember.Route.extend ChooseLocationRouteMixin, ManageTypedInsertionsRouteMixin,
  setupController: (controller, model)->
    region_id = controller.get("controllers.routes/new.region.id")
    model = this.store.find("subregion-name", { parent_location_id: region_id  })
    controller.set('model', model)


`export default RoutesNewSubregionRoute`