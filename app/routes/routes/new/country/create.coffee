`import Ember from 'ember'`
`import CreateLocationRouteMixin from "outdoor-client/mixins/create-location-route"`
`import ManageTypedInsertionsRouteMixin from 'outdoor-client/mixins/manage-typed-insertions-route'`

RoutesNewCountryCreateRoute = Ember.Route.extend CreateLocationRouteMixin, ManageTypedInsertionsRouteMixin,
  model: (params, transition)->
    return  Ember.$.getJSON("/countries_list")
  setupController: (controller, tempModel)->
    model = this.store.createRecord('country')
    model.set "gallery", this.store.createRecord('gallery')


    model.get("gallery.albums").addObject this.store.createRecord('album')

    controller.set "locationsList", tempModel
    controller.set 'model', model
    

`export default RoutesNewCountryCreateRoute`