`import Ember from 'ember'`
`import ChooseLocationRouteMixin from "outdoor-client/mixins/choose-location-route"`
`import ManageTypedInsertionsRouteMixin from 'outdoor-client/mixins/manage-typed-insertions-route'`

RoutesNewRegionRoute = Ember.Route.extend ChooseLocationRouteMixin, ManageTypedInsertionsRouteMixin,
  model: ()->
    this.container.lookup('controller:routes.new')
    country_id = this.container.lookup('controller:routes.new').get("country.id")
    return this.store.find("region-name", { parent_location_id: country_id })
    


`export default RoutesNewRegionRoute`