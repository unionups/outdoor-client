`import Ember from 'ember'`
`import ChooseLocationRouteMixin from "outdoor-client/mixins/choose-location-route"`
`import ManageTypedInsertionsRouteMixin from 'outdoor-client/mixins/manage-typed-insertions-route'`


RoutesNewCountryRoute = Ember.Route.extend ChooseLocationRouteMixin, ManageTypedInsertionsRouteMixin,
  model: (params, transition)->
    return  this.store.find("country-name")
  
`export default RoutesNewCountryRoute`