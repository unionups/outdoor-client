`import Ember from 'ember'`
`import CreateLocationRouteMixin from "outdoor-client/mixins/create-location-route"`
`import ManageTypedInsertionsRouteMixin from 'outdoor-client/mixins/manage-typed-insertions-route'`

RoutesNewRegionCreateRoute = Ember.Route.extend CreateLocationRouteMixin, ManageTypedInsertionsRouteMixin,
  model: (params, transition)->
    return this.store.createRecord('region')
    

`export default RoutesNewRegionCreateRoute`