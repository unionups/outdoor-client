`import Ember from 'ember'`

loadGoogleMap = (resolveWith) ->
  $meta = Ember.$('meta[name="google-map-url"]')
  if $meta.length
    src = $meta.attr('content')
    $meta.remove()
    return promise = new Ember.RSVP.Promise (resolve, reject)->
      window.__emberGoogleMapLoaded__ = Ember.run.bind ()->
        promise = null
        window.__emberGoogleMapLoaded__ = null
        resolve(resolveWith)
      
      Ember.$.getScript(src + '&callback=__emberGoogleMapLoaded__').fail (jqXhr)->
        promise = null
        window.__emberGoogleMapLoaded__ = null
        reject(jqXhr)
  else if promise 
    # we already have the promise loading the script, use it as the core promise to wait for but
    # resolve to what was given this time
    return promise.then ()->
      return resolveWith
  else 
    # no need to do anything, resolve directly
    return Ember.RSVP.resolve(resolveWith)
  

`export default loadGoogleMap`
