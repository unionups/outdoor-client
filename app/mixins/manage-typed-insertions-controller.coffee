`import Ember from 'ember'`

ManageTypedInsertionsControllerMixin = Ember.Mixin.create
  typedInsertion: null
  confirm: null
  locatonPropertyName: "model"
  transportHubTypes: ["airport", "seaport", "railway", "bus", "taxi", "hitchhiking", "public", "other"]
  accommodationTypes: ["hotel", "guesthouse", "wildcamping", "other"]
  file: null
  dataURL: null

  actions:
    addInsertion: (typeKey)->
      this.set "typedInsertion", this.store.createRecord(typeKey)
      this.send "openModal"
      return true

    createTransportHub: ()->
      lpn = this.get "locatonPropertyName"
      this.get(lpn).get("transportHubs").addObject this.get("typedInsertion")
      this.send "closeModal"

    createContact: ()->
      lpn = this.get "locatonPropertyName"
      this.get(lpn).get("contacts").addObject this.get("typedInsertion")
      this.send "closeModal"

    createAccommodation: ()->
      lpn = this.get "locatonPropertyName"
      this.get(lpn).get("accommodations").addObject this.get("typedInsertion")
      this.send "closeModal"

    createPhoto: ()->
      lpn = this.get "locatonPropertyName"
      album = this.get(lpn).get("gallery.albums.firstObject")
      photo = this.get("typedInsertion")
      # file = document.getElementById('file-field').files[0]
      photo.set "image", this.get("file")
      photo.set "dataURL", this.get("dataURL")
      album.get("photos").addObject photo
      this.set "dataURL", null
      this.send "closeModal"

    destroyInsertion: (insertion)->
      this.set "confirm",  insertion
      this.send "openModal"
      return true

    confirmed: (insertion)->
      this.send "closeModal"
      insertionTypeKey = insertion.constructor.typeKey
      lpn = this.get "locatonPropertyName"
      this.get(lpn).get(insertionTypeKey+"s").removeObject(insertion) 

    cancelConfirm: ()->
      this.set "confirm", null
      this.send "closeModal"

    editInsertion: (insertion)->
      this.set "typedInsertion", insertion
      this.send "openModal"
      return true

    insertionSelectChanged: (insertionType)->
      this.set "typedInsertion.insertionType", insertionType

`export default ManageTypedInsertionsControllerMixin`
