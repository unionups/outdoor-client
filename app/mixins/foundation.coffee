`import Ember from 'ember'`

FoundationMixin = Ember.Mixin.create
  didInsertElement: ()->
    this._super();
    Ember.run.scheduleOnce 'afterRender', this, this.afterRenderElement
  afterRenderElement: ()->
    this.get("parentView").$().foundation()
    $(document).foundation()
    # $(document).foundation('topbar', 'reflow')


`export default FoundationMixin`
