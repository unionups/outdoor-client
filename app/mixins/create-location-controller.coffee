`import Ember from 'ember'`

CreateLocationControllerMixin = Ember.Mixin.create
  locationType: "country"
  parentLocationType: "country"
  photosBinding: "model.gallery.albums.firstObject.photos"
  

  needs: (->
    _this=this
    return ["routes/new/"+_this.get("locationType") , "routes/new"]
    ).property()

  actions:
    locationNameAdded: (locationNameObj)->
      this.set "model.name", locationNameObj.name
      this.set "model.cca2", locationNameObj.cca2 if locationNameObj.cca2?
      this.set "model.cca3", locationNameObj.cca3 if locationNameObj.cca3?
      this.set "model.latitude", locationNameObj.latitude
      this.set "model.longitude", locationNameObj.longitude
      

    createLocation: ()->
      this.send "loading"
      locationType = this.get "locationType"
      parentLocationType = this.get("parentLocationType")
      unless parentLocationType is locationType
        parentLocation = this.get("controllers.routes/new."+parentLocationType)
        this.set 'model.'+parentLocationType , parentLocation
      photos =  this.get "photos"
      this.get('model').save().then (location)=>
        location.get("gallery.albums.firstObject.photos").addObjects photos
        location.get("gallery.albums.firstObject.photos").save()
        this.store.find(locationType+"-name", {parent_location_id: if parentLocation then parentLocation.get("id") else null }).then (location_names)=>
          this.set "controllers.routes/new/"+locationType+".model", location_names
          this.set "controllers.routes/new/"+locationType+".location", location
          this.set "controllers.routes/new/"+locationType+".locationId", location.get("id")
          this.set "controllers.routes/new/"+locationType+".isLocationChoosed", true
          this.send 'finished'
          this.transitionToRoute "routes.new."+locationType
      , (error)=>
        this.send "error", error
  
`export default CreateLocationControllerMixin`
