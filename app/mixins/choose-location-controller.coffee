`import Ember from 'ember'`

ChooseLocationControllerMixin = Ember.Mixin.create
  needs: "routes/new"
  locationType: null
  selectPrompt: (->
    return this.t "routes.new.choose_"+this.get("locationType")+"_ph"
    ).property()
  isLocationChoosed: false
  isNewLocation: false
  isPropertiesChanged: false
  isCountry: (->
    this.get("locationType") is "country"
    ).property()
  isRegion: (->
    this.get("locationType") is "region"
    ).property()
  isSubregion: (->
    this.get("locationType") is "subregion"
    ).property()
  isPropertiesChangedObserver: (->
    prop = "controllers.routes/new.is"+this.get("locationType").charAt(0).toUpperCase()+this.get("locationType").substr(1)+"PropertiesChanged"
    if this.get("isPropertiesChanged") then this.set(prop , true) else this.set(prop , true)
    ).observes "isPropertiesChanged"
  locationId: null
  location: null
  isCurrentStep: (->
    return  this.get("controllers.routes/new.current") is this.get("locationType")
    ).property "controllers.routes/new.current"
  defaultLocationName: (->
    return this.get("model").findBy("name", this.get("location.name"))
    ).property "location"
  nextClasses: (->
    return if this.get("isLocationChoosed") then "button" else "button disabled"
    ).property("isLocationChoosed")
  actions:
    createLocation: ()->
      this.set "isLocationChoosed", false
      this.set "locationId", null
      # this.set "location", null
      this.set "isNewLocation", true
      this.transitionToRoute "routes.new."+this.get("locationType")+".create"
    locationChoose: (locationId)->
      this.send("locationChoosed", this.get("locationType"), locationId) if this.get("isLocationChoosed")
      return false
    propertyChanged: ()->
      this.set "isPropertiesChanged", true
      return false

`export default ChooseLocationControllerMixin`
