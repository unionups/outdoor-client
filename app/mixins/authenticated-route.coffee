`import Ember from 'ember'`

AuthenticatedRouteMixin = Ember.Mixin.create
  ability: Ember.K
  roles: [ "user", "moderator", "admin" ]
  
  roleIsCan: (base_role)->
    if this.session.get("isAuthenticated")?
      roles = this.get "roles"
      if roles.indexOf(base_role) <= roles.indexOf(this.session.get "content.role") 
        return true
      else
        this.send "error", new Ember.Error(this.t 'errors.no_page_permission')
        return false
    else 
      this.send "login"
      return false
  beforeModel: (transition)->
    unless this.roleIsCan(this.get "ability") 
      transition.abort()
      this.transitionTo("root")
    this._super()



`export default AuthenticatedRouteMixin`
