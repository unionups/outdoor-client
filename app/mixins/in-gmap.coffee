`import Ember from 'ember'`

InGmapMixin = Ember.Mixin.create
  gmapComponent: (->
    parentView = this.get('parentView')
    while parentView?
      if parentView.get('mapCanvas')?
        return parentView
      parentView = parentView.get('parentView')
    return Em.assert(false, 'Cannot find gm-map component')
    ).property 'parentView'

`export default InGmapMixin`

