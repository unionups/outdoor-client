`import Ember from 'ember'`

LocationSerializerMixin = Ember.Mixin.create DS.EmbeddedRecordsMixin,
  attrs: 
    transportHubs: { serialize: 'records', deserialize: 'ids' }
    contacts: { serialize: 'records', deserialize: 'ids' }
    gallery:  {embedded: 'always'} # { serialize: 'records', deserialize: 'records' }
  keyForAttribute: (attr) ->
    switch attr
      when "transportHubs"  then  "transport_hubs_attributes"
      when "contacts"       then  "contacts_attributes"
      when "accommodations" then  "accommodations_attributes"
      when "gallery" then  "gallery_attributes"
      else
        this._super(attr)

`export default LocationSerializerMixin`
