`import Ember from 'ember'`

CreateLocationRouteMixin = Ember.Mixin.create
  deactivate: ()->
    model = this.controller.get("model")
    model.deleteRecord() unless model.id?
    this.send "closeModal"
    this.controller.set "controllers.routes/new/"+this.controller.get("locationType")+".isNewLocation", false
  

`export default CreateLocationRouteMixin`
