`import Ember from 'ember'`

ManageTypedInsertionsRouteMixin = Ember.Mixin.create
  actions:
    addInsertion: (typeKey)->
      this.render ('typed-insertion/add' + typeKey.charAt(0).toUpperCase() + typeKey.substr(1)),
        into: "application", 
        outlet: 'modal'
      return false
    destroyInsertion: ()->
      this.render 'confirm',
        into: "application", 
        outlet: 'modal'
      return false
    editInsertion: (insertion)->
      this.send "addInsertion", insertion.constructor.typeKey
      return false

`export default ManageTypedInsertionsRouteMixin`
