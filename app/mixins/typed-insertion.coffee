`import Ember from 'ember'`
`import DS from 'ember-data'`

TypedInsertionMixin = Ember.Mixin.create
  insertionType:  DS.attr "string"
  name: DS.attr "string"
  description: DS.attr "string"

`export default TypedInsertionMixin`