`import Ember from 'ember'`

ChooseLocationRouteMixin = Ember.Mixin.create
  actions:
    selectChanged: (location_id)->
      this.send "loading"
      if this.controller.get("isPropertiesChanged")
        this.controller.get("location").rollback() 
        this.controller.set "isPropertiesChanged", false
      this.store.find(this.controller.get("locationType"), location_id).then (location)=>
        this.controller.set "locationId", location_id
        this.controller.set "isLocationChoosed", true
        this.controller.set "location", location
        this.send 'finished'
      , =>
        this.send 'finished'          
      return false

`export default ChooseLocationRouteMixin`
