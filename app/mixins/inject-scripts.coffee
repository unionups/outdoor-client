`import Ember from "ember"`

InjectScriptsMixin = Ember.Mixin.create 
  injectScripts:  []
  beforeModel: ()->
    promises = []
    this.get("injectScripts").forEach (scriptSRC)->
      promises.push new Ember.RSVP.Promise (resolve, reject)->
        script    = document.createElement 'script'
        script.type   = 'text/javascript'
        script.async  = true
        script.src    = scriptSRC
        script.onload = ()-> 
          console.log scriptSRC
          console.log "is loaded"
          resolve()
        script.onerror = ()->
          console.log scriptSRC
          console.log "is rejected"
          reject()

      
        document.getElementsByTagName('head')[0].appendChild(script);
       
  

    return new Ember.RSVP.all promises
 
`export default InjectScriptsMixin`
  

