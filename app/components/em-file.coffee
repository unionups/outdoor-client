`import Ember from 'ember'`
`import InputComponent from 'ember-idx-forms/input'`

EmFileComponent = InputComponent.extend 
  type: "file"
  input: null
  file: null
  dataURL: null
  didInsertElement: ()->
    input = Foundation.utils.S(".em-file-field-wrapper input")
    input.change (event)=>
      files = event.target.files
      file = files[0]
      this.set "file", file
      fileReader = new FileReader()
      fileReader.addEventListener "load", (event)=>
        dataURL = event.target.result
        this.set "dataURL", dataURL

      fileReader.readAsDataURL(file)
    this.set "input", input


  actions:
    addFile: ()->
      this.get("input").click()
      return false

`export default EmFileComponent`
