`import Ember from 'ember'`
`import FoundationMixin from '../mixins/foundation'`


RevealModalComponent = Ember.Component.extend FoundationMixin,
  layoutName: "reveal-modal"
  classNames: ["reveal-modal"]
  classNameBindings: ["size"]
  size: undefined
  attributeBindings: ["id", "dataReveal:data-reveal"]
  dataReveal: "data-reveal"
  id: undefined
  open: false
  changeState: ()->
    open = this.get("open")
    if open
      this.$().foundation('reveal', 'open')
    else
      this.$().foundation('reveal', 'close')

  openObserver: (->
    this.changeState()
    ).observes("open")
  
  didInsertElement: ()->
    this._super();
    Ember.run.scheduleOnce 'afterRender', this, this.afterRenderElement
  afterRenderElement: ()->
    _this=this
    this.changeState()
    this.$(document).on 'close.fndtn.reveal', '[data-reveal]', ()->
      _this.send "close"

  actions:
    close: ()->
      this.sendAction "close"



`export default RevealModalComponent`