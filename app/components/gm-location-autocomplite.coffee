`import Ember from 'ember'`
`import SimpleSelectComponent from 'outdoor-client/components/simple-select'`
`import InGmapMixin from "outdoor-client/mixins/in-gmap"`



GmLocationAutocompliteComponent = SimpleSelectComponent.extend InGmapMixin, 
  init: ()->
    this._super()
    this.set "autocompliteService", new google.maps.places.AutocompleteService()


  content: null
  searchInputElement: null
  predictions: null

  mapBinding: "gmapComponent.map"


  latitudeBinding: "gmapComponent.latitude"
  longitudeBinding: "gmapComponent.longitude"


  infoWindowBinding: "gmapComponent.infoWindow"
  markerBinding: "gmapComponent.marker"

  updateMap: ()->
    this.get("gmapComponent").send "cleanMap"

  didInsertElement: ()->
    this.set "placesService", new google.maps.places.PlacesService(this.get("map"))
    this.updateMap()

  mapObserver: (->
    this.updateMap()
    ).observes "map"

  
  choosen: (->
    selectElement = this.get("selectElement")
    $(selectElement).chosen()
    searchInputElement = $("div.chosen-search").children()
    this.set "searchInputElement",  searchInputElement
    searchInputElement.on "input propertychange", Ember.run.bind(this, this.searchInputChange)
    $(selectElement).on "DOMSubtreeModified", Ember.run.bind(this, this.selectDidChange)
    ).observes "selectElement"



  searchInputChange: ()->
    autocompliteService = this.get "autocompliteService"
    if autocompliteService? and this.get("searchInputElement")[1].value?
      opts= 
        input: this.get("searchInputElement")[1].value
        types: ['(regions)']
        componentRestrictions: { country: 'ua'}

      predictionsPromise = new Ember.RSVP.Promise (resolve, reject)->
        autocompliteService.getQueryPredictions opts, (predictions, status)->
          if status isnt google.maps.places.PlacesServiceStatus.OK 
            reject(new Error "Cannot load prediction") 
          else
            resolve(predictions)

      predictionsPromise.then (predictions)=>
        
        predictions = predictions.map (prediction)->
          return {name: prediction.terms[0].value, place_id: prediction.place_id}
        this.set "predictions", predictions
        predictions = predictions.map (prediction)->
          return prediction.name
        this.set "content", predictions



  selectDidChange: ()->
    selectElement = this.get("selectElement")
    $(selectElement).trigger "chosen:updated"


  changeObserver: (->
    val = this.get "value"
    placesService = this.get "placesService"
    map = this.get("map")

    prediction = this.get("predictions").findBy "name", val
    if prediction.place_id?
      placesService.getDetails { placeId: prediction.place_id }, (place, status)=>
        if status is google.maps.places.PlacesServiceStatus.OK
          latitude = place.geometry.location.lat()
          longitude = place.geometry.location.lng()
          this.updateMap()

          this.set "location.name", prediction.name
          this.set "latitude", latitude
          this.set "location.latitude", latitude
          this.set "longitude", longitude
          this.set "location.longitude", longitude

          marker = this.get("marker")
          infoWindow = this.get("infoWindow")
          marker.setPosition place.geometry.location
          marker.setIcon
            url: ("https://maps.gstatic.com/mapfiles/ms2/micons/POI.png")
            size: new google.maps.Size(71, 71)
            origin: new google.maps.Point(0, 0)
            anchor: new google.maps.Point(17, 34)
            scaledSize: new google.maps.Size(35, 35)
          marker.setVisible true

          infoWindow.setContent '<div><strong><h5>'+prediction.name+'</h5></strong></div>'
          infoWindow.open map, marker

    ).observes "value"

`export default GmLocationAutocompliteComponent`
