`import Ember from 'ember'`
`import SimpleSelectComponent from 'outdoor-client/components/simple-select'`
`import InGmapMixin from "outdoor-client/mixins/in-gmap"`

GmSelectComponent = SimpleSelectComponent.extend InGmapMixin, 
  
  content: null
  searchInputElement: null


  map:  Ember.computed.alias("gmapComponent.map")
  geoJsonData:  Ember.computed.alias("gmapComponent.geoJsonData")
  latitude:  Ember.computed.alias("gmapComponent.latitude")
  longitude:  Ember.computed.alias("gmapComponent.longitude")

  infoWindow:  Ember.computed.alias("gmapComponent.infoWindow")
  marker:  Ember.computed.alias("gmapComponent.marker")

  updateMap: ()->
    if this.get("map")?
      defaultSelection = this.get("defaultSelection")
      unless defaultSelection? 
        this.get("gmapComponent").send "cleanMap"
       else
        this.set("value", defaultSelection.get("id")) 



  didInsertElement: ()->
    this.updateMap()


  mapObserver: (->
    this.updateMap()
    ).observes "map"


  changeObserver: (->
    val = this.get("value")

    if val?
      map = this.get("map")
      if map?
        this.get("gmapComponent").send "cleanMap"
        if $.isNumeric(val)
          this.sendAction "action", val
          obj = this.get("content").findBy "id", val
          if obj?
            this.set "latitude", obj.get("latitude")
            this.set "longitude", obj.get("longitude")
            name = obj.get("name")
            if obj.get("cca2")?
              cca2 = obj.get("cca2")
              cca3 = obj.get("cca3").toLowerCase()

        else
          obj = this.get("content").findBy "name", val
          if obj?
            this.sendAction "action", obj
            this.set "latitude", obj.latitude
            this.set "longitude", obj.longitude
            name = obj.name
            if obj.cca2?
              cca2 = obj.cca2
              cca3 = obj.cca3.toLowerCase()
        
        
        infoWindow = this.get("infoWindow")
        marker = this.get("marker")
        marker.setIcon
          url: ("https://maps.gstatic.com/mapfiles/ms2/micons/POI.png")
          size: new google.maps.Size(71, 71)
          origin: new google.maps.Point(0, 0)
          anchor: new google.maps.Point(17, 34)
          scaledSize: new google.maps.Size(35, 35)

        marker.setPosition new google.maps.LatLng(this.get("latitude"), this.get("longitude"))
        marker.setVisible true
        if cca2?
          Ember.$.getJSON "countries-geojson/"+cca3+".geo.json", (geoJson)=>
            this.set "geoJsonData", map.data.addGeoJson(geoJson)
          infoWindow.setContent '<div><strong><h5>'+name+'</h5></strong>'+cca2+'<br><img src="countries-geojson/'+cca3+'.svg" style="width: 70px"></div>'
        else
          infoWindow.setContent '<div><strong><h5>'+name+'</h5></strong></div>'
        infoWindow.open map, marker
        

    ).observes "value"
  


  choosen: (->
      selectElement = this.get("selectElement")
      $(selectElement).chosen()
    ).observes "selectElement"


    
`export default GmSelectComponent`



