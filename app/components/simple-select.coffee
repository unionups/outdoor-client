`import Ember from 'ember'`

SimpleSelectComponent = Ember.Component.extend
  action: "selectChanged"
  selectElement: null
  selectView:  Ember.Select.extend 
    attributeBindings: ["chosenPlaceholder:data-placeholder"]
    chosenPlaceholderBinding: "parentView.prompt"
    valueBinding: "parentView.value"
    contentBinding: 'parentView.content'
    optionLabelPathBinding: 'parentView.optionLabelPath'
    optionValuePathBinding: 'parentView.optionValuePath'
    promptBinding: 'parentView.prompt'
    selectionBinding: "parentView.selection"
    didInsertElement: ()->
      this.set "parentView.selectElement", this.get("element")
  optionValuePath: 'content'
  optionLabelPath: 'content'
  selection: (->
    return this.get "defaultSelection"
    ).property "defaultSelection"


  changeObserver: (->
    val = this.get("value")
    this.sendAction "action", val if val
    ).observes "value"

`export default SimpleSelectComponent`
