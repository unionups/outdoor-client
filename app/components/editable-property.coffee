`import Ember from 'ember'`

EditablePropertyComponent = Ember.Component.extend
  classNames: ["panel", "editable-property-panel"]
  classNameBindings: ["class", "isHower:callout"]
  isHower: false
  isEdit: false
  class: null
  property: null
  propertyTemp: null
  action: "propertyChanged"

  mouseEnter: ()->
    this.set "isHower", true
  mouseLeave: ()->
    unless this.get("isEdit")
      this.set "isHower", false

  actions:
    onEdit: ()->
      this.set "propertyTemp", this.get "property"
      this.set "isEdit", true
    endEdit: ()->
      this.set "isHower", false
      propertyTemp = this.get "propertyTemp"
      unless this.get("property") is propertyTemp
        this.set "property", propertyTemp
        this.sendAction 'action' 
      this.set "isEdit", false


`export default EditablePropertyComponent`
