`import Ember from 'ember'`

GmMapComponent = Ember.Component.extend
  mapCanvas: Ember.View.extend
    classNames: ["map-canvas"]
    didInsertElement: ()->
      this.set "parentView.mapElement", this.get("element")
      
  mapElement: null
  insertMap: (->
    mapElement = this.get("mapElement")
    if mapElement?
      mapOptions = 
        scrollwheel: false
        center:  new google.maps.LatLng(this.get("latitude"), this.get("longitude"))  
        zoom: this.get("zoom")
        mapTypeId: this.get("mapTypeId")
      map = new google.maps.Map(mapElement, mapOptions)
      this.set "infoWindow", new google.maps.InfoWindow()
      this.set "marker", new google.maps.Marker
        map: map
        anchorPoint: new google.maps.Point(0, -29)
      this.set "map", map
    ).observes "mapElement"

  centerMap: (->
    map = this.get("map")
    map.setCenter(new google.maps.LatLng(this.get("latitude"), this.get("longitude"))) if map?
    ).observes "latitude", "longitude"

  control: null

  latitude: 0
  longitude: 0
  zoom: 5
  mapTypeId: google.maps.MapTypeId.ROADMAP

  map: null
  geoJsonData: null
  infoWindow: null
  marker: null

  actions:
    cleanMap: ()->
      map = this.get("map")
      if map?
        geoJsonData = this.get("geoJsonData")
        infoWindow = this.get("infoWindow")
        marker = this.get("marker")
        map.data.remove(geoData) for geoData in geoJsonData if geoJsonData?
        infoWindow.close() 
        marker.setVisible false




`export default GmMapComponent`
