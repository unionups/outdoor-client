`import Ember from 'ember'`
`import MasonryGrid from 'ember-masonry-grid/masonry'`

EmGalleryComponent = MasonryGrid.extend
  # attributeBindings: ["id"]
  # id: "gallery"
  layoutName: "em-gallery"
  elements: Ember.A()
  fullElement: null
  fullImageClasses: "full"

  randomWidth: (->
    thumbSizes = [10, 20, 40]
    sid = (Math.random() * 3 ) << 0
    return  "width: "+thumbSizes[sid]+"%"
    ).property().volatile()



  itemsChanged: (->
    Ember.run.once(this, 'itemsPrepended')
    ).observes('items.length')
  itemsPrepended: ()->
    # elems = []
    # fragment = document.createDocumentFragment()
    # elem = document.createElement('a')
    # elem.className = '.item'
    # elem.style.width = this.get("randomWidth")+"%"

    addedItem = this.get "items.lastObject"
    # addedItem.set "src", if addedItem.get("dataURL") then addedItem.get("dataURL") else addedItem.get("image_random_thumb")
    addedItem.set "style", "opacity: 0;"
    this.get("elements").unshiftObject addedItem
    # img = document.createElement('img')
    # src = if addedItem.get("dataURL") then addedItem.get("dataURL") else addedItem.get("image_random_thumb")
    # img.src = addedItem.get "dataURL"
    # elem.appendChild img
    # fragment.appendChild elem 
    # elems.push elem 
    # this.get("element").insertBefore( fragment, this.get("element").firstChild )
    # this.$().masonry( 'prepended', elems )
    this.$().imagesLoaded ()=>
      # _this.$().masonry(options);
      # _this.$().masonry('reloadItems');
      addedItem.set "style", "opacity: 1;"
      this.initializeMasonry()
    # this.$().masonry('reloadItems');

  actions:
    showFull: (element)->
      fullImageClasses = this.get "fullImageClasses"
      fic = fullImageClasses.split(' ')
      fic.removeObject("show")
      this.set "fullImageClasses", fic.join(" ") 
      this.set "fullElement", element
      fic.push "show"
      this.set "fullImageClasses", fic.join(" ")  
      # this.$("img.full").load ()->
      #   console.log "show"
      #   this.addClass("show")
      

    toPrev: ()->
      elements = this.get "elements"
      fullEl = this.get "fullElement"
      ci = elements.indexOf(fullEl) 
      ci = elements.length  if ci is 0
      this.set "fullElement", elements.objectAt(ci-1)
    toNext: ()->
      elements = this.get "elements"
      fullEl = this.get "fullElement"
      ci = elements.indexOf(fullEl) + 1
      ci = 0 if elements.length is ci
      this.set "fullElement", elements.objectAt(ci)

    close: ()->
      this.set "fullElement", null
   
`export default EmGalleryComponent`
