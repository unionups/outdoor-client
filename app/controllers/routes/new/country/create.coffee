`import Ember from 'ember'`
`import CreateLocationControllerMixin from "outdoor-client/mixins/create-location-controller"`
`import ManageTypedInsertionsControllerMixin from 'outdoor-client/mixins/manage-typed-insertions-controller'`

RoutesNewCountryCreateController = Ember.Controller.extend CreateLocationControllerMixin, ManageTypedInsertionsControllerMixin,
  locationType: "country"
  locationsList: null
  

  
`export default RoutesNewCountryCreateController`