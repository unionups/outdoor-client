`import Ember from 'ember'`
`import ChooseLocationControllerMixin from "outdoor-client/mixins/choose-location-controller"`
`import ManageTypedInsertionsControllerMixin from 'outdoor-client/mixins/manage-typed-insertions-controller'`

RoutesNewRegionController = Ember.Controller.extend ChooseLocationControllerMixin, ManageTypedInsertionsControllerMixin,
  locationType: "region"
  locatonPropertyName: "location"



`export default RoutesNewRegionController`