`import Ember from 'ember'`
`import CreateLocationControllerMixin from "outdoor-client/mixins/create-location-controller"`
`import ManageTypedInsertionsControllerMixin from 'outdoor-client/mixins/manage-typed-insertions-controller'`

RoutesNewRegionCreateController = Ember.Controller.extend CreateLocationControllerMixin, ManageTypedInsertionsControllerMixin,
  locationType: "region"

`export default RoutesNewRegionCreateController`