`import Ember from 'ember'`
`import CreateLocationControllerMixin from "outdoor-client/mixins/create-location-controller"`
`import ManageTypedInsertionsControllerMixin from 'outdoor-client/mixins/manage-typed-insertions-controller'`

RoutesNewSubregionCreateController = Ember.Controller.extend CreateLocationControllerMixin, ManageTypedInsertionsControllerMixin,
  locationType: "subregion"
  parentLocationType: "region"
  
`export default RoutesNewSubregionCreateController`