`import Ember from 'ember'`
`import ChooseLocationControllerMixin from "outdoor-client/mixins/choose-location-controller"`
`import ManageTypedInsertionsControllerMixin from 'outdoor-client/mixins/manage-typed-insertions-controller'`

RoutesNewSubregionController = Ember.Controller.extend ChooseLocationControllerMixin, ManageTypedInsertionsControllerMixin,
  locationType: "subregion"
  locatonPropertyName: "location"



`export default RoutesNewSubregionController`