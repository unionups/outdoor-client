`import Ember from 'ember'`
`import ChooseLocationControllerMixin from "outdoor-client/mixins/choose-location-controller"`
`import ManageTypedInsertionsControllerMixin from 'outdoor-client/mixins/manage-typed-insertions-controller'`

RoutesNewCountryController = Ember.Controller.extend ChooseLocationControllerMixin, ManageTypedInsertionsControllerMixin,
  locationType: "country"
  locatonPropertyName: "location"
  

`export default RoutesNewCountryController`