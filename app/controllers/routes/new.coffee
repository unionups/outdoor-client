`import Ember from 'ember'`


RoutesNewController = Ember.Controller.extend 
  needs: ["application"]
  isCountryChoosed: false
  isRegionChoosed: false
  isSubregionChoosed: false 
  isCountryPropertiesChanged: false
  isRegionPropertiesChanged: false
  isSubregionPropertiesChanged: false
  country: null
  region: null
  subregion: null

  current: "country"
  


  countryState: (->
    return "active" if this.get("current") is "country"
    ).property "current"
  regionState: (->
    return "active" if this.get("current") is "region"
    ).property "current"
  subregionState: (->
    return "active" if this.get("current") is "subregion"
    ).property "current"

  actions:
 
    onCountry: ()->
      this.transitionToRoute "routes.new.country" if this.get("isCountryChoosed") or this.get("current") is "country"
 
    onRegion: ()->
      if this.get("isRegionChoosed") or this.get("current") is "region"
        this.transitionToRoute "routes.new.region" 
      else
        this.set "controllers.application.modalContent", "You must choose COUNTRY first!"
        this.send "openModal"

    onSubregion: ()->
      if this.get("isSubregionChoosed") or this.get("current") is "subregion"
        this.transitionToRoute "routes.new.subregion" 
      else 
        this.set "controllers.application.modalContent", "You must choose COUNTRY and REGION first!"  
        this.send "openModal"


`export default RoutesNewController`