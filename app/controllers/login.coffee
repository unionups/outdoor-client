`import Ember from 'ember'`
`import LoginControllerMixin from 'simple-auth/mixins/login-controller-mixin'`
`import EmberValidations from 'ember-validations'`


LoginController = Ember.Controller.extend LoginControllerMixin, EmberValidations.Mixin,
  init: ()->
    this._super()
    this.set "validations.identification.format.message", this.t('auth_forms.email_validate')

  needs: ["application"]
  authenticator: 'simple-auth-authenticator:devise'
  actions:
    authenticate: ()->
      this.send "loading"
      this._super().then =>
        this.send "finished"
        this.send "closeModal"
      , =>
        _this.send "finished"
    closeModal: ()->
      return true

LoginController.reopen
  identificationHasValue: (->
    not this.get('identification')?.length
    ).property('identification')
  submitClasses: (->
    return if this.get("isValid") then "button right" else "button right disabled"
    ).property("isValid")
  validations: 
    identification:
      presence: true
      format: { with: /@/, allowBlank: true }
    password:
      presence: true
      length: { minimum: 6, allowBlank: true }    


`export default LoginController`