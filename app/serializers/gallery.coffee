`import DS from 'ember-data'`

GallerySerializer = DS.ActiveModelSerializer.extend DS.EmbeddedRecordsMixin,
  attrs: 
    albums: {embedded: 'always'} # { serialize: 'records', deserialize: 'ids' }
    # location: {serialize: false, deserialize: 'ids'}
  keyForAttribute: (attr) ->
    switch attr
      when "albums" then  "albums_attributes"
      else
        this._super(attr)

`export default GallerySerializer`