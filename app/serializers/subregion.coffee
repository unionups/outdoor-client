`import DS from 'ember-data'`
`import LocationSerializerMixin from '../mixins/location-serializer'`

SubregionSerializer = DS.ActiveModelSerializer.extend DS.EmbeddedRecordsMixin,
  attrs: 
    transportHubs: { serialize: 'records', deserialize: 'ids' },
    contacts: { serialize: 'records', deserialize: 'ids' },
    accommodations: { serialize: 'records', deserialize: 'ids' }
  keyForAttribute: (attr) ->
    switch attr
      when "transportHubs" then  "transport_hubs_attributes"
      when "contacts"       then  "contacts_attributes"
      when "accommodations"       then  "accommodations_attributes"
      else
        this._super(attr)

`export default SubregionSerializer`