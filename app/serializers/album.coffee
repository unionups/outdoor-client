`import DS from 'ember-data'`

AlbumSerializer = DS.ActiveModelSerializer.extend DS.EmbeddedRecordsMixin,
  attrs: 
    photos: { serialize: false, deserialize: 'ids' }
    gallery: { serialize: false, deserialize: 'ids' }
  keyForAttribute: (attr) ->
    switch attr
      when "photos" then  "photos_attributes"
      else
        this._super(attr)

`export default AlbumSerializer`