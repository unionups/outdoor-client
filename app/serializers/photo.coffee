`import DS from 'ember-data'`

PhotoSerializer = DS.ActiveModelSerializer.extend DS.EmbeddedRecordsMixin,
  attrs: 
    albums: { serialize: 'id', deserialize: 'ids' }
    dataURL: { serialize: false }
    image_random_thumb: { serialize: false }

`export default PhotoSerializer`