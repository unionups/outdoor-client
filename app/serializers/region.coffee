`import DS from 'ember-data'`
`import LocationSerializerMixin from '../mixins/location-serializer'`

RegionSerializer = DS.ActiveModelSerializer.extend DS.EmbeddedRecordsMixin, LocationSerializerMixin, {}

`export default RegionSerializer`