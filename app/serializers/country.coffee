`import DS from 'ember-data'`
`import LocationSerializerMixin from '../mixins/location-serializer'`

CountrySerializer = DS.ActiveModelSerializer.extend DS.EmbeddedRecordsMixin, LocationSerializerMixin, {}
  
`export default CountrySerializer`
