`import Ember from 'ember'`
`import config from './config/environment'`

Router = Ember.Router.extend
  location: config.locationType


Router.map ()->
  this.resource "routes", ()->
    this.resource "routes.new", { path: '/new' }, ()->
      this.resource "routes.new.country",{ path: '/country' }, ()->
        this.route "create"
      this.resource "routes.new.region", { path: '/region' },()->
        this.route "create"
      this.resource "routes.new.subregion", { path: '/subregion' },()->
        this.route "create"

`export default Router`
