`import Ember from 'ember'`
`import DS from 'ember-data'`
`import TypedInsertionMixin from '../mixins/typed-insertion'`
`import EmberValidations from 'ember-validations'`

Accommodation= DS.Model.extend TypedInsertionMixin,  EmberValidations.Mixin,
  accommodationType: Ember.computed.alias "insertionType"
  
Accommodation.reopen
  validations: 
    accommodationType:
      presence: true
      inclusion: { in: ["hotel", "guesthouse", "wildcamping", "other"] }
    name:
      presence: true
    description:
      presence: true

`export default Accommodation`