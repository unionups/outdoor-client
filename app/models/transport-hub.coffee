`import Ember from 'ember'`
`import DS from 'ember-data'`
`import TypedInsertionMixin from '../mixins/typed-insertion'`
`import EmberValidations from 'ember-validations'`

TransportHub = DS.Model.extend TypedInsertionMixin,  EmberValidations.Mixin,
  hubType: Ember.computed.alias "insertionType"
  
TransportHub.reopen
  validations: 
    hubType:
      presence: true
      inclusion: { in: ["airport", "seaport", "railway", "bus", "taxi", "hitchhiking", "public", "other"] }
    name:
      presence: true
    description:
      presence: true

`export default TransportHub`