`import DS from 'ember-data'`
`import EmberValidations from 'ember-validations'`

Subregion = DS.Model.extend EmberValidations.Mixin,
  name: DS.attr "string"
  description: DS.attr "string"
  latitude: DS.attr "number"
  longitude: DS.attr "number"
  
  region: DS.belongsTo('region', { async: true })
  transportHubs: DS.hasMany( "transportHub", {async: true})
  contacts:  DS.hasMany('contact', {async: true})
  accommodations:  DS.hasMany('accommodation', {async: true})
  

Subregion.reopen
  validations: 
    name:
      presence: true
      # format: { with: /^[a-zA-Z\s]*$/, allowBlank: false, message: 'must be letters only'  }
 

`export default Subregion`