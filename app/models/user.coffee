`import DS from 'ember-data'`

User = DS.Model.extend
  email: DS.attr "string"
  role: DS.attr "string"
  lang: DS.attr "string"
    
  chrole: (base_role)->
    roles = ["user", "moderator", "admin" ]
    return roles.indexOf("base_role") <= roles.indexOf(this.get("role"))
  isUser: (->
    return this.get("session").isAuthenticated
    ).property("session.isAuthenticated")
  isModerator: (->  
    return this.chrole("moderator")
    ).property("session.isAuthenticated")
  isAdmin: (->
    return this.chrole("admin")
    ).property("session.isAuthenticated")

    

`export default User`
