`import DS from 'ember-data'`

CountryName = DS.Model.extend
  name: DS.attr "string"
  latitude: DS.attr "number"
  longitude: DS.attr "number"
  cca2: DS.attr "string"
  cca3: DS.attr "string"


`export default CountryName`