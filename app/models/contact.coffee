`import Ember from 'ember'`
`import DS from 'ember-data'`
`import TypedInsertionMixin from '../mixins/typed-insertion'`
`import EmberValidations from 'ember-validations'`

Contact = DS.Model.extend TypedInsertionMixin, EmberValidations.Mixin,
  phone: Ember.computed.alias "name"
  insertionType: DS.attr "string", 
    defaultValue: "phone"
Contact.reopen
  validations: 
    phone:
      presence: true
      # format: { with: /^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$/, allowBlank: false, message: 'must be phone number'  }
    description:
      presence: true

`export default Contact`