`import DS from 'ember-data'`
`import EmberValidations from 'ember-validations'`

Photo = DS.Model.extend EmberValidations.Mixin,
  name:  DS.attr "string"
  description:  DS.attr "string"
  image:  DS.attr "file"
  dataURL: DS.attr "string"
  image_random_thumb: DS.attr "string"

  
  albums: DS.hasMany('album')

Photo.reopen
  validations: 
    image:
      presence: true


`export default Photo`
