`import DS from 'ember-data'`

SubregionName = DS.Model.extend
  name: DS.attr "string"
  latitude: DS.attr "number"
  longitude: DS.attr "number"


`export default SubregionName`