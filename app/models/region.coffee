`import DS from 'ember-data'`
`import EmberValidations from 'ember-validations'`

Region = DS.Model.extend EmberValidations.Mixin,
  name: DS.attr "string"
  description: DS.attr "string"
  latitude: DS.attr "number"
  longitude: DS.attr "number"
  
  country: DS.belongsTo('country', { async: true })
  subregions: DS.hasMany('subregion', {async: true})
  transportHubs: DS.hasMany( "transportHub", {async: true})
  contacts:  DS.hasMany('contact', {async: true})
  

Region.reopen
  validations: 
    name:
      presence: true
      # format: { with:  /^[a-zA-Z\s]*$/, allowBlank: false, message: 'must be letters only'  }
 

`export default Region`