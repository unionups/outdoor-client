`import DS from 'ember-data'`
`import EmberValidations from 'ember-validations'`

Country = DS.Model.extend EmberValidations.Mixin,
  name: DS.attr "string"
  cca2: DS.attr "string"
  cca3: DS.attr "string"
  latitude: DS.attr "number"
  longitude: DS.attr "number"
  description: DS.attr "string"
  visaInfo: DS.attr "string"
  governmentPage: DS.attr "string"
  transportInfo: DS.attr "string"

  
  regions: DS.hasMany('region', {async: true})
  transportHubs: DS.hasMany( "transportHub", {async: true})
  contacts:  DS.hasMany('contact', {async: true})
  gallery: DS.belongsTo("gallery")
  
Country.reopen
  validations: 
    name:
      presence: true
      # format: { with:  /^[a-zA-Z\s]*$/, allowBlank: false, message: 'must be letters only'  }
    # isoCode:
    #   presence: true
    #   length: [2, 5]
    #   format: { with: /^([a-zA-Z])+$/, allowBlank: false, message: 'must be letters only' }
    governmentPage:
      format: { with: /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/, allowBlank: false, message: 'must be url path' }

   

`export default Country`