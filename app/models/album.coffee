`import DS from 'ember-data'`

Album = DS.Model.extend 
  name:  DS.attr "string"
  description:  DS.attr "string"
  gallery: DS.belongsTo('gallery')
  photos: DS.hasMany('photo')

`export default Album`
