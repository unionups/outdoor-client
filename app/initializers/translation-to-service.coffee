# Takes two parameters: container and app


initialize = (container, app) ->
  app.deferReadiness()
  # session = container.lookup 'simple-auth-session:main'
  defaultLocale = app.defaultLocale
  
  Ember.$.getJSON "/locales/"+defaultLocale+".json", (translation)->
    tr = Ember.K
    tr.translation = translation
    app.register 'i18n:translation', tr  , {instantiate: false}

    app.advanceReadiness()




TranslationToServiceInitializer =
  name: 'translation-to-service'
  # after: 'simple-auth'
  initialize: initialize


`export default TranslationToServiceInitializer`
