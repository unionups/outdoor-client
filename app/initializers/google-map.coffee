`import loadGoogleMap from 'outdoor-client/utils/load-google-map'`

initialize = (container, app) ->
  app.register 'util:load-google-map', loadGoogleMap, {instantiate: false}
  app.inject 'route', 'loadGoogleMap', 'util:load-google-map'
  # app.register 'route', 'foo', 'service:foo'

GoogleMapInitializer =
  name: 'google-map'
  initialize: initialize

`export {initialize}`
`export default GoogleMapInitializer`
