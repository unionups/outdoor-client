/* global require, module */

var EmberApp = require('ember-cli/lib/broccoli/ember-app');
var env = process.env.EMBER_ENV;
var config = require('./config/environment')(env);
var yaml2json = require('broccoli-yaml');
var mergeTrees = require('broccoli-merge-trees');
var pickFiles = require('broccoli-static-compiler');


var googleAPISetup = function(config){
  var src, content = '', google = config.googleMap || {}, params = [];
  src = "https://maps.googleapis.com/maps/api/js";

  // if (google.version) {
  //   params.push('v=' + encodeURIComponent(google.version));
  // }  

  if (google.signed_in) {
    params.push('signed_in=true');
  }

  if (google.apiKey) {
    params.push('key=' + encodeURIComponent(google.apiKey));
  }  
  
  if (google.libraries && google.libraries.length) {
    params.push('libraries=' + encodeURIComponent(google.libraries.join(',')));
  }

  src += '?' + params.join('&');
  if (google.lazyLoad) {
    content = '<meta name="google-map-url" content="' + src + '">';
  }
  else {
    content = '<script type="text/javascript" src="' + src + '"></script>';
  }
  return content;
}


var app = new EmberApp({
  'ember-cli-foundation-sass': {
    'modernizr': true,
    'fastclick': true,
    'foundationJs': ['topbar','reveal']
  },
  minifyHTML: {
    collapseWhitespace : true,
    removeComments     : true,
    minifyJS           : true,
    minifyCSS          : true
  },
 inlineContent: {
    'gm-api' : {
      content: googleAPISetup(config) 
    },
    "choosen-js" : {
      content: "<script src='chosen.jquery.min.js' type='text/javascript'></script>"
    }
    // "choosen-css" : {
    //   content: "<link rel='stylesheet' href='chosen.min.css'>"
    // }
  }
});

var fixImport = function(path, opts){
  // fix path with windows back slash 
  for (var key in opts){
    opts[key] = opts[key].replace(/\\/g, '/');
  }
  app.import( path.replace(/\\/g, '/'), opts );
};

fixImport('bower_components/foundation-icon-fonts/foundation-icons.eot', {
    destDir: 'assets/foundation-icon-fonts'
});
fixImport('bower_components/foundation-icon-fonts/foundation-icons.ttf', {
    destDir: 'assets/foundation-icon-fonts'
});
fixImport('bower_components/foundation-icon-fonts/foundation-icons.svg', {
    destDir: 'assets/foundation-icon-fonts'
});
fixImport('bower_components/foundation-icon-fonts/foundation-icons.woff', {
    destDir: 'assets/foundation-icon-fonts'
});


var localesThree = yaml2json('config/locales', { space: 2});
var locales = pickFiles(localesThree, {
  srcDir: '/',
  files: ['**/*.json'],
  destDir: '/locales'
});

module.exports = mergeTrees([app.toTree(), locales]);
