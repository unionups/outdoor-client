/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'outdoor-client',
    environment: environment,
    baseURL: '/',
    // locationType: "hashchange",//'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true

      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
      defaultLocale: 'en'
    }
  };

  ENV['simple-auth'] = {
    authorizer: 'simple-auth-authorizer:devise',
    crossOriginWhitelist: ['localhost:4200']
  }

  ENV['simple-auth-devise'] = {
    identificationAttributeName: "email"
  }

  ENV['googleMap'] = {
    // version: "3.13",
    libraries: ['places'],
    lazyLoad: false,
    signed_in: true
    // apiKey: 'AbCDeFgHiJkLmNoPqRsTuVwXyZ'
  }

  ENV["contentSecurityPolicyHeader"] = 'Disabled-Content-Security-Policy'

  // ENV["contentSecurityPolicy"] = {
  // 'default-src': "'none'",
  // 'script-src': "'self' 'unsafe-inline' 'unsafe-eval'   maps.googleapis.com maps.gstatic.com",
  // // 'font-src': "'self' http://fonts.gstatic.com", 
  // // 'connect-src': "'self' https://api.mixpanel.com http://custom-api.local", 
  // 'img-src': "'self' 'unsafe-inline' 'unsafe-eval'  localhost:4200 maps.googleapis.com maps.gstatic.com csi.gstatic.com"
  // // 'style-src': "'self' 'unsafe-inline' http://fonts.googleapis.com", 
  // // 'media-src': "'self'"
  // }

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {

  }

  return ENV;
};
